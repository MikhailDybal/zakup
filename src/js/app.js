import jQuery from 'jquery';

(function ($) {

  const setTabsBehavior = function () {
    let $wrapper = $('.tab-wrapper');
    let $tabContent = $wrapper.find('.tab-content');
    let $allTabs = $wrapper.find('.tab-content > div');
    let $tabMenu = $wrapper.find('.main-menu li');
    let $line = $('<div class="line"></div>').appendTo($tabMenu);

    $($wrapper).hover(() => {
      $tabContent.show();
    }, () => {
      $tabContent.hide();
    });

    $tabMenu.each(function (i) {
      $(this).attr('data-tab', 'tab' + i);
    });

    $allTabs.each(function (i) {
      $(this).attr('data-tab', 'tab' + i);
    });

    $tabMenu.on('mouseover', function () {
      let dataTab = $(this).data('tab');
      let $getWrapper = $(this).closest($wrapper);
      $getWrapper.find($tabMenu).removeClass('active');
      $(this).addClass('active');
      $getWrapper.find($allTabs).hide();
      $getWrapper.find($allTabs).filter('[data-tab=' + dataTab + ']').show();
    });
  };

  $(document).ready(function () {
    // Show tab content on hover
    setTabsBehavior();

  });//end ready

})(jQuery);