require('./sass/reset.scss');
require('./sass/fonts.scss');
require('./sass/variables.scss');
require('./sass/mixins.scss');
require('./sass/bootstrap-custom.scss');
require('./sass/main.scss');

// Components
require('./sass/components/main-menu.scss');
require('./sass/components/footer.scss');
require('./sass/components/bottom-links.scss');
require('./sass/components/partners.scss');
require('./sass/components/info.scss');

const app = require('./js/app.js');
require('bootstrap');
